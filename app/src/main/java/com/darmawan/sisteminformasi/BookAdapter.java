package com.darmawan.sisteminformasi;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.HelpViewHolder> {
    private List<Book> dataList;
    Context ctx;

    public BookAdapter(List<Book> dataList, Context ctx) {
        this.dataList = dataList;
        this.ctx = ctx;
    }

    @Override
    public HelpViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_book, parent, false);
        return new HelpViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HelpViewHolder holder, final int position) {
        holder.tvNama.setText(dataList.get(position).getNama());
        holder.tvTeks.setText(dataList.get(position).getTeks());
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class HelpViewHolder extends RecyclerView.ViewHolder {
        private TextView tvNama;
        private TextView tvTeks;

        public HelpViewHolder(View itemView) {
            super(itemView);
            tvNama = itemView.findViewById(R.id.tvNama);
            tvTeks = itemView.findViewById(R.id.tvTeks);
        }
    }
}