package com.darmawan.sisteminformasi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    LinearLayout studio1, studio2, studio3, studio4, studio5, studio6;
    ImageView contact1, web1, map1, guestbook1;
    ImageView contact2, web2, map2, guestbook2;
    ImageView contact3, web3, map3, guestbook3;
    ImageView contact4, web4, map4, guestbook4;
    ImageView contact5, web5, map5, guestbook5;
    ImageView contact6, web6, map6, guestbook6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setElevation(0);
        getSupportActionBar().setTitle("Game Studio");

        studio1 = findViewById(R.id.studio1);
        studio2 = findViewById(R.id.studio2);
        studio3 = findViewById(R.id.studio3);
        studio4 = findViewById(R.id.studio4);
        studio5 = findViewById(R.id.studio5);
        studio6 = findViewById(R.id.studio6);
        studio1.setOnClickListener(this);
        studio2.setOnClickListener(this);
        studio3.setOnClickListener(this);
        studio4.setOnClickListener(this);
        studio5.setOnClickListener(this);
        studio6.setOnClickListener(this);
    }

    private void gotoDetail(int id) {
        DetailActivity.id_studio = id;
        DetailActivity.deskripsi_studio = getResources().getStringArray(R.array.deskripsi)[id-1];
        DetailActivity.nama_studio = getResources().getStringArray(R.array.nama)[id-1];
        DetailActivity.web_studio = getResources().getStringArray(R.array.web)[id-1];
        DetailActivity.telp_studio = getResources().getStringArray(R.array.contact)[id-1];
        DetailActivity.lat_studio = Double.valueOf(getResources().getStringArray(R.array.lat)[id-1]);
        DetailActivity.lng_studio = Double.valueOf(getResources().getStringArray(R.array.lng)[id-1]);

        Log.d("MainActivity", DetailActivity.lat_studio + ", " + DetailActivity.lng_studio);

        Intent intent = new Intent(this, DetailActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.studio1:
                gotoDetail(1);
                break;
            case R.id.studio2:
                gotoDetail(2);
                break;
            case R.id.studio3:
                gotoDetail(3);
                break;
            case R.id.studio4:
                gotoDetail(4);
                break;
            case R.id.studio5:
                gotoDetail(5);
                break;
            case R.id.studio6:
                gotoDetail(6);
                break;
        }
    }
}
