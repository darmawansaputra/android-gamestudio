package com.darmawan.sisteminformasi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GalleryFragment extends Fragment {
    private GridView gridview = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);

        gridview = (GridView) view.findViewById(R.id.gridview);

        List<Integer> temp = new ArrayList<>();

        if(DetailActivity.id_studio == 1) {
            temp = new ArrayList<Integer>(Arrays.asList(R.drawable.agate_1, R.drawable.agate_2, R.drawable.agate_3));
        }
        else if(DetailActivity.id_studio == 2) {
            //touc
            temp = new ArrayList<Integer>(Arrays.asList(R.drawable.touchten_1, R.drawable.touchten_2, R.drawable.touchten_3));
        }
        else if(DetailActivity.id_studio == 3) {
            //arsa
            temp = new ArrayList<Integer>(Arrays.asList(R.drawable.arsanesia_1, R.drawable.arsanesia_2));
        }
        else if(DetailActivity.id_studio == 4) {
            //mojji
            temp = new ArrayList<Integer>(Arrays.asList(R.drawable.mojiken_1));
        }
        else if(DetailActivity.id_studio == 5) {
            //digital
            temp = new ArrayList<Integer>(Arrays.asList(R.drawable.digital_1, R.drawable.digital_2));
        }
        else if(DetailActivity.id_studio == 6) {
            //ggame
            temp = new ArrayList<Integer>(Arrays.asList(R.drawable.gameloft_1, R.drawable.gameloft_2, R.drawable.gameloft_3));
        }

        final List<Integer> imgs = temp;

        gridview.setAdapter(new ImageAdapter(getContext(), imgs));

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                Intent i = new Intent(getContext(), BigImageActivity.class);
                BigImageActivity.image = imgs.get(position);
                startActivity(i);
            }
        });

        return view;
    }
}