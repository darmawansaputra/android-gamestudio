package com.darmawan.sisteminformasi;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DataHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "guestbook.db";
    private static final int DATABASE_VERSION = 1;

    public DataHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table guestbook (id integer primary key AUTOINCREMENT, id_studio integer, nama text, teks text);";
        db.execSQL(sql);

        //1 = agate
        //2 = touchten
        //3 = arsanesia
        //4 = mojiken
        //5 = digital happines
        //6 = gameloft

        sql = "INSERT INTO guestbook (id_studio, nama, teks) VALUES (1, 'Wawan', 'Hallo paman');";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {

    }

    public void addBook(int id, Book book){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("id_studio", id);
        values.put("nama", book.getNama());
        values.put("teks", book.getTeks());

        db.insert("guestbook", null, values);
        db.close();
    }

    public List<Book> getBook(int id) {
        List<Book> bookList = new ArrayList<>();
        String selectQuery = "SELECT * FROM guestbook WHERE id_studio = " + id;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Book book = new Book(cursor.getString(2), cursor.getString(3));
                bookList.add(book);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return bookList;
    }
}