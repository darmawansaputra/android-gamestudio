package com.darmawan.sisteminformasi;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

public class OverviewFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_overview, container, false);

        TextView tvDeskripsi = view.findViewById(R.id.txtDeskripsiStudio);
        TextView tvTelp = view.findViewById(R.id.txtTelp);
        tvDeskripsi.setText(DetailActivity.deskripsi_studio);
        tvTelp.setText(DetailActivity.telp_studio);

        tvTelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + DetailActivity.telp_studio));
                startActivity(intent);
            }
        });

        ImageView img = view.findViewById(R.id.imgStudio);

        int drawable = 0;

        switch (DetailActivity.id_studio) {
            case 1:
                drawable = R.drawable.logo_agate;
                break;
            case 2:
                drawable = R.drawable.logo_touchten;
                break;
            case 3:
                drawable = R.drawable.logo_arsanesia;
                break;
            case 4:
                drawable = R.drawable.logo_mojiken;
                break;
            case 5:
                drawable = R.drawable.logo_digital;
                break;
            case 6:
                drawable = R.drawable.logo_gameloft;
                break;
        }

        img.setImageDrawable(getResources().getDrawable(drawable));

        return view;
    }
}