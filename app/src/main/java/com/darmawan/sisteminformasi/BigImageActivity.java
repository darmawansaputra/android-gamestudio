package com.darmawan.sisteminformasi;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class BigImageActivity extends AppCompatActivity {
    public static int image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_big_image);

        ImageView img = findViewById(R.id.imageView);
        img.setImageDrawable(getResources().getDrawable(image));
    }
}
