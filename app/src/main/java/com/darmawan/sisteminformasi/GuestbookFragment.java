package com.darmawan.sisteminformasi;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class GuestbookFragment extends Fragment {
    RecyclerView recyclerView;
    DataHelper dbcenter;
    View view;
    BookAdapter adapter;

    EditText txtNama, txtTeks;
    Button btnKirim;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_guestbook, container, false);

        dbcenter = new DataHelper(getContext());

        recyclerView = view.findViewById(R.id.recyclerBook);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        txtNama = view.findViewById(R.id.txtNama);
        txtTeks = view.findViewById(R.id.txtTeks);
        btnKirim = view.findViewById(R.id.btnKirim);
        btnKirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbcenter.addBook(DetailActivity.id_studio, new Book(txtNama.getText().toString(), txtTeks.getText().toString()));
                LoadList();

                txtNama.setText("");
                txtTeks.setText("");
            }
        });

        LoadList();

        return view;
    }

    public void LoadList(){
        adapter = new BookAdapter(dbcenter.getBook(DetailActivity.id_studio), getContext());
        recyclerView.setAdapter(adapter);
    }
}