package com.darmawan.sisteminformasi;

public class Book {
    private String nama;
    private String teks;

    public Book(String nama, String teks) {
        this.nama = nama;
        this.teks = teks;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTeks() {
        return teks;
    }

    public void setTeks(String teks) {
        this.teks = teks;
    }
}
